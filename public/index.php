<?php
/**
 * Public index.php. This file brings everything together and displays the page.
 * You can configure apache to point to the public/ directory.
 */

define("INDEX", true);
define("ERROR_REPORTING", true);

// error reporting
if(ERROR_REPORTING){
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}else{
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
    error_reporting(0);
}

$root = dirname(__FILE__)."/..";

// includes
require_once "$root/vendor/autoload.php";
require_once "$root/core/func.php";
require_once "$root/core/AppSettings.php";
require_once "$root/core/App.php";
require_once "$root/core/Page.php";
require_once "$root/core/DB.php";

// include every file in pages folder
foreach(glob("$root/pages/*.class.php") as $file) {
    $file = str_replace("\0", "", $file);
    if (is_file($file)) require_once $file;
}

// main include
require_once "$root/main.php";
