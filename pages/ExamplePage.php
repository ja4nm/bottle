<?php
/**
 * Example hello page made with simple php file which should be included in router callback function.
 */
defined("INDEX") or die("No direct access allowed!");

echo App::getInstance()->getTwig()->render("hello.twig", ["name" => $name]);
