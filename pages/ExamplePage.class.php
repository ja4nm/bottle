<?php
/**
 * Example hello page with twig template made by extending Page class.
 * Classes in folder page/ with extension .class.php are automatically included.
 */

class ExamplePage extends Page
{

    public function show($name = null){
        echo App::getInstance()->getTwig()->render("hello.twig", ["name" => $name]);
    }

}

?>