<?php
/**
 * Abstract page class.
 * Extend it to make you own pages, then call show() method in router callback function.
 */

abstract class Page {

    public abstract function show();

}
