<?php
/**
 * DB.php
 * Class for manipulating with database.
 *
 * by Jan M
 */

class DB extends PDO{

	private static $_instance = null;

	/**
	 * Constructor.
	 */
	public static function getInstance(){
		if(self::$_instance == null)
			self::$_instance = new DB();

		return self::$_instance;
	}
	public function __construct(){}

	/**
	 * Connect to db.
	 */
	public function connect($host, $db, $un, $pass){
		try{
			parent::__construct("mysql:host=$host;dbname=$db;charset=utf8", $un, $pass);
			var_dump("ok");
		}catch (PDOException $e){
			if(ERROR_REPORTING)
				echo $e->getMessage();

			die();
		}
	}

	/**
	 * Check if value exists in a table.
	 */
	public function valueExists($table, $col, $value){
		$s = $this->prepare("SELECT 1 FROM `$table` WHERE $col = ?;");
		$s->bindParam(1, $value);
		$s->execute();

		return $s->rowCount() > 0;
	}

	/**
	 * Insert into database.
	 */
	public function insert($table, $values){

		//prepare params
		$cols_str = "";
		$values_str = "";
		foreach($values as $key => $value){
			$cols_str .= $key.",";
			$values_str .= "?,";
		}
		$cols_str = substr($cols_str, 0, -1);
		$values_str = substr($values_str, 0, -1);

		//prepare sql
		$sql = "INSERT INTO `$table` ($cols_str) VALUES ($values_str);";

		//bind params and execute
		$values = array_values($values);
		$s = $this->prepare($sql);
		$s->execute($values);
	}

	/**
	 * Update rows.
	 */
	public function update($table, $values, $where, $where_args = array()){

		//prepare values
		$set_str = "";
		foreach($values as $key => $value){
			$set_str .= "$key=?,";
		}
		$set_str = substr($set_str, 0, -1);

		//prepare sql
		$sql = "UPDATE $table SET $set_str WHERE $where;";

		//bind value params
		$params = array_values($values);
		$params = array_merge($params, $where_args);
		
		//execute
		$s = $this->prepare($sql);
		$s->execute($params);
	}

	/**
	 * Delete rows.
	 */
	public function delete($table, $where, $where_args = array()){
		$sql = "DELETE FROM $table WHERE $where;";
		$s = $this->prepare($sql);
		$s->execute($where_args);
	}

}
