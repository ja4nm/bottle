<?php
/**
 * Useful global functions.
 */

/**
 * Get current request url.
 */
function phpSlef(){
    $url = 'http';

    if(!isset($_SERVER["HTTPS"])) $_SERVER["HTTPS"] = "off";
    if($_SERVER["HTTPS"] == "on") $url .= "s";

    $url .= "://";

    if($_SERVER["SERVER_PORT"] != "80")
        $url .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    else
        $url .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

    $url = rtrim($url, "/");
    return $url;
}

/**
 * Get post / get / request / session variable.
 * If is not set return $default.
 */
function getPostVar($key, $default = null){
    return isset($_POST[$key])? $_POST[$key] : $default;
}
function getGetVar($key, $default = null){
    return isset($_GET[$key])? $_GET[$key] : $default;
}
function getRequestVar($key, $default = null){
    return isset($_REQUEST[$key])? $_REQUEST[$key] : $default;
}
function getSessionVar($key, $default = null){
    return isset($_SESSION[$key])? $_SESSION[$key] : $default;
}
