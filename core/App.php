<?php
/**
 * Main singleton application class.
 * You can routing and template engine through this class.
 */

class App
{

    /** @var App */
    private static $_instance;

    /** @var AppSettings */
    public $settings;

    /** @var AltoRouter */
    private $router;

    /** @var Twig_Environment */
    private $twig;

    /** @var callable */
    private $errorResponse;

    /**
     * App constructor singleton.
     */
    private function __construct(){
        $this->router = new AltoRouter();

        // Specify our Twig templates location
        $twigLoader = new Twig_Loader_Filesystem(__DIR__.'/../templates');
        $this->twig = new Twig_Environment($twigLoader);
    }
    public static function getInstance(){
        if(self::$_instance == null) self::$_instance = new self();
        return self::$_instance;
    }

    /**
     * Init application with settings
     * @param AppSettings $settings
     */
    public function init(AppSettings $settings){
        $this->settings = $settings;
        session_start();

        // router init
        $base = rtrim($this->settings->base, "/");
        $this->router->setBasePath($base);

        // add default global values and functions to twig
        $this->getTwig()->addGlobal("appSettings", $this->settings);
        $this->getTwig()->addFunction(new Twig_SimpleFunction("phpSelf", function (){
            return phpSlef();
        }));
        $this->getTwig()->addFunction(new Twig_SimpleFunction("baseUrl", function ($scheme = null){
            return $this->getBaseUrl($scheme);
        }));

        // mysql connect
        if($this->settings->useSql){
            $db = DB::getInstance();
            $db->connect($this->settings->sqlHost, $this->settings->sqlDb, $this->settings->sqlUser, $this->settings->sqlPass);
        }
    }

    /**
     * Process matched request
     */
    public function run(){
        $match = $this->router->match();

        if ($match && is_callable($match['target'] )) {
            call_user_func_array($match['target'], $match['params']);
        } else {
            call_user_func_array($this->errorResponse, []);
        }
    }

    /**
     * 404 response callback is called when router cant match any other pages.
     * @param callable $response
     */
    public function map404response($response){
        $this->errorResponse = $response;
    }

    /**
     * @return AppSettings
     */
    public function getSettings(){
        return $this->settings;
    }

    /**
     * @return AltoRouter
     */
    public function getRouter(){
        return $this->router;
    }

    /**
     * @return Twig_Environment
     */
    public function getTwig(){
        return $this->twig;
    }

    /**
     * Get base url which usually points to home page.
     * @return string
     */
    public function getBaseUrl($scheme = null){
        if ($scheme == null){
            $scheme = isset($_SERVER["HTTP_X_FORWARDED_PROTO"])? $_SERVER["HTTP_X_FORWARDED_PROTO"] : $_SERVER["REQUEST_SCHEME"];
        }
        $uri = $scheme."://".$_SERVER["HTTP_HOST"]."/".trim($this->settings->base, "/");
        $uri = trim($uri, "/");
        return $uri;
    }

}
