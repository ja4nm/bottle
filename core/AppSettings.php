<?php
/**
 * Application settings like page root path and database information.
 * You can also extend it and make your own.
 */

class AppSettings{

    public $base = "/public";

    public $useSql = false;
    public $sqlHost = "localhost";
    public $sqlDb = "";
    public $sqlUser = "root";
    public $sqlPass = "root";

    public function __construct(){}

}

