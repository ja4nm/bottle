<?php
/**
 * Main application file where you define app settings, twig settings and routes.
 */

defined("INDEX") or die("No direct access allowed!");

// application settings
$settings = new AppSettings();
$settings->base = "/bottle/public";
$settings->useSql = false;
$settings->sqlHost = "localhost";
$settings->sqlDb = "";
$settings->sqlUser = "root";
$settings->sqlPass = "root";

// initialise application
$app = App::getInstance();
$app->init($settings);

// add some global template variables or functions
$app->getTwig()->addGlobal("appColor", "#396589");
$app->getTwig()->addFunction(new Twig_SimpleFunction("getAppName", function (){
    return "Example website";
}));

// default error / 404 response page
$app->map404response(function (){
    echo App::getInstance()->getTwig()->render("404.twig");
});

// home page
// (showing page directly from function)
$app->getRouter()->map( 'GET', '/', function() {
    echo App::getInstance()->getTwig()->render('base.twig');
});

// hello1/your_name
// (showing page from Page class file)
$app->getRouter()->map( 'GET', '/hello1/[:name]?', function($name = "jan") {
    $page = new ExamplePage();
    $page->show($name);
});

// hello2/your_name
// (showing page as included file)
$app->getRouter()->map( 'GET', '/hello2/[:name]?', function($name = "jan") {
    require_once "pages/ExamplePage.php";
});

// match and handle request
$app->run();
