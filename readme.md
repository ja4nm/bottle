# Bottle PHP framework #

Bottle is a lightweight PHP framework that helps you quickly write powerful organised web applications. It is inspired by Python's Flask framework, it uses AltoRouter routing engine and popular Twig template engine.

One can read more about this frameworks here:
* Flask: http://flask.pocoo.org/
* AltoRouter: http://altorouter.com/
* Twig: https://twig.symfony.com/

## General project structure ##

General project structure is briefly described below but there is nothing keeping you from expending that structure to your own needs. Note: all Page class files (which have extension .class.php) in folder `pages/` are automatically included.

    core/ (framework files which generaly should not be modified)
    pages/ (php files for logic of different pages)
    public/ (public filder which displays the page to the user)
        index.php
        static/ (folder with static files like css, js or images)
    templates/ (folder which cointains all Twig templates)
    main.php (main application file where we define all application settings and routes)


## Getting started ##

Getting started with Bottle framework is very easy. You just need to clone the repo and call `composer install` in the project root directory. You can read more about composer here: https://getcomposer.org/. Of course I assume that you have Apache already installed. I have done my best to document the code and provide some examples in the project skeleton.

    cd /var/www/html/
    git clone https://gitlab.com/ja4nm/bottle.git
    cd bottle/
    composer install
    # modify setting in main.php
    # navigate to http://localhost/bottle/public/

## Setting up Apache ##

You can also configure apache to point directly to the `public/` directory. Then you will be able to access the page through url http://localhost/.

1) Modify file `/etc/apache2/sites-available/000-default.conf` to look like this:

    <VirtualHost *:80>
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html/bottle/public/
    
        <Directory /var/www/html/>
            Options Indexes FollowSymLinks
            AllowOverride All
            Require all granted
        </Directory>
    </VirtualHost>

2) Restart Apache using command `sudo /etc/init.d/apache2 restart`

3) Set `$settings->base = "/"` in file `main.php` in project root directory.

<hr />
By Jan M - https://janm.si/
